# Aquí se definen las etapas en las que se divide nuestro ciclo CI/CD y será lo que se muestre en la ejecución de las tareas en la web de GitLab.
stages:
  - test
  - build
  - deploy

# Comienza la etapa de integración la cual realiza los tests.
test:
  # Se asocia esta tarea a la etapa test.
  stage: test

  # Imagen que se va a usar para montar en un contenedor de un runner compartido de GitLab.
  image: node:10.16.0

  # Serie de comandos a realizar en orden riguroso de ejecución que definen la tarea principal de la etapa. Primero entramos en el directorio api, para lanzar la instalación de las dependencias del backend. Ejecutamos el lanzamiento de los test y el lint, que permite corregir los errores de sintaxis en el código y finalmente, realizamos la misma operación para el frontend.
  script:
    - cd api && npm install
    - npm run test
    - npm run lint
    - cd .. && cd backoffice && npm install
    - npm run lint

# Comienza la etapa de despliegue, primero con la construcción del código a desplegar y luego con el despliegue y ejecución del mismo.
build:
  # Se asocia esta tarea a la etapa build.
  stage: build

  # Al indicar un tag en una etapa, ésta se vincula con el runner que tenga esta “tag” en su configuración, en este caso el runner con el tag “cicdhlc” será el encargado de realizar esta estapa.
  tags:
    - cicdhlc

  # Serie de comandos a realizar previos a la ejecución de los comandos de las tareas principales. Aquí realizamos la limpieza de contenedores y las imágenes de artefactos dockerizados. También realizamos el login en Docker Hub para subir los artefactos dockerizados.
  before_script:
    - docker container rm -f builds
    - docker image rmi -f builds
    - docker image rmi -f gestion-crud-backend
    - docker image rmi -f gestion-crud-frontend
    - cat ~/my_password.txt | docker login --username cicdhlc --password-stdin

  # Aquí realizaremos la creación de una imagen que usaremos para la elaboración del código final de producción (se realizará de forma automática mediante su dockerfile). A continuación, lanzaremos dicha imagen, copiaremos los artefactos con el código de producción a nuestro directorio de trabajo del runner y, finalmente, crearemos dos imágenes con los artefactos de cada parte del proyecto en su interior, los cuales subiremos a Docker Hub.
  script:
    - docker build -f Dockerfile-builds -t builds .
    - docker container run -d --name=builds builds
    - docker cp builds:/home/node/app/api/dist backend
    - docker cp builds:/home/node/app/frontend frontend
    - docker build -f Dockerfile-backend -t gestion-crud-backend .
    - docker build -f Dockerfile-frontend -t gestion-crud-frontend .
    - docker tag gestion-crud-backend cicdhlc/gestion-crud-backend
    - docker push cicdhlc/gestion-crud-backend
    - docker tag gestion-crud-frontend cicdhlc/gestion-crud-frontend
    - docker push cicdhlc/gestion-crud-frontend

  # Serie de comandos a realizar después de terminar la tarea principal de la etapa, en este caso, salir de la sesión de Docker Hub.
  after_script:
    - docker logout

  # Esta etiqueta se usa para que esta tarea/etapa de la pipeline, solo se ejecute cuando se realice un tag en el código del proyecto.
  only:
    - tags

deploy:
  # Se asocia esta tarea a la etapa deploy.
  stage: deploy

  # Tag que vincula esta tarea/etapa con el runner que contenga la misma tag para que éste la realice.
  tags:
    - cicdhlc

  # En esta etapa realizaremos la copia del fichero docker-compose mediante scp al servidor donde desplegaremos el proyecto. A continuación lanzamos los dos artefactos dockerizados mediante un comando por ssh.
  script:
    - scp -o "StrictHostKeyChecking no" -i ~/lopezrodriguez docker-compose.yml root@146.59.155.113:/home/hlc
    - ssh -o "StrictHostKeyChecking no" -i ~/lopezrodriguez root@146.59.155.113 "docker-compose -f /home/hlc/docker-compose.yml down"
    - ssh -o "StrictHostKeyChecking no" -i ~/lopezrodriguez root@146.59.155.113 "docker-compose -f /home/hlc/docker-compose.yml up -d"
    - ssh -o "StrictHostKeyChecking no" -i ~/lopezrodriguez root@146.59.155.113 "docker container exec -dit backend sh -c 'node /home/node/app/backend/main.js'"

  # Esta etiqueta se usa para que esta tarea/etapa de la pipeline, solo se ejecute cuando se realice un tag en el código del proyecto.
  only:
    - tags
