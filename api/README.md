# activities

## Description

description

* [] Panel de administración (Backoffice)
  * [] Autenticación de usuarios para acceder a un panel de administración (Un único rol)
  * [] Gestionar actividades( Alta actividades, Baja actividades, Consulta Actividades, Modificar Actividades)
  * [] Consultar los inscritos, modificar los inscriptos, insertar inscriptos.
  * [] Consultar los precios, modificar los precios, insertar nuevos precios.
* [] Aplicación pública (web)
  * [] Inscripción de participantes en actividades

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

