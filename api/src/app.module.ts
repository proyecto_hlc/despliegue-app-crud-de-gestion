import { ActivityModule } from './modules/activity/activity.module';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { MailerModule } from '@nestjs-modules/mailer';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    ActivityModule,
    MailerModule.forRoot({
      transport: {
        host: 's5.closemarketing.net',
        port: 587,
        secure: false,
        auth: {
          user: 'algunusuario',
          pass: 'algunpassword',
        },
      },
      defaults: {
        from: '"FAN  " <noresponder@alguncorreo.com>',
      },
      template: {
        dir: __dirname + '/templates',
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
