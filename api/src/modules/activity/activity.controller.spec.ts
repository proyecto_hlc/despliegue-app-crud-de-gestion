import { Test } from '@nestjs/testing';
import { ActivityService } from './services/activity.service';
import { ActivityController } from './activity.controller';
import {
  ACTIVITY_REPOSITORY_TOKEN,
  ENROLL_REPOSITORY_TOKEN,
} from '../../shared/config/database.tokens.constants';
import { Activity } from './entities/activity.entity';
import { MailerService } from '@nestjs-modules/mailer';
import { CECAService } from '../../shared/services/ceca.service';
import { ENV } from '../../env';

describe('ActivityController', () => {
  let activityService;
  let activityController;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [ActivityController],
      providers: [
        ActivityService,
        {
          provide: ACTIVITY_REPOSITORY_TOKEN,
          useFactory: () => ({}),
        },
        {
          provide: ENROLL_REPOSITORY_TOKEN,
          useFactory: () => ({}),
        },
        {
          provide: MailerService,
          useValue: {
            mailerServiceData: jest.fn(),
          },
        },
        CECAService,
        ENV,
      ],
    }).compile();

    activityController = moduleRef.get<ActivityController>(ActivityController);
    activityService = moduleRef.get<ActivityService>(ActivityService);
  });

  describe('findById', () => {
    it('should invoke the activityService findByID method', async () => {
      const params = {
        id: 1,
      };
      const mock = {
        activity: {
          id: 1,
          name: 'Actividad1',
        } as Activity,
      };
      jest
        .spyOn(activityService, 'findById')
        .mockImplementation(() => mock.activity);

      const activity = await activityController.findById(params.id);

      expect(activity).toBe(mock.activity);
      expect(activityService.findById).toHaveBeenCalledWith(params.id);
    });
  });
});
