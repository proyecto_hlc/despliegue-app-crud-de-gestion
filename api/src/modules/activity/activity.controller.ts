import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { ActivityService } from './services/activity.service';
import { Activity } from './entities/activity.entity';
import { ActivityDTO } from './dtos/activity.dto';
import { ActivityUpdateDto } from './dtos/activity-update.dto';
import { RolesGuard } from '../../shared/guards/roles.guard';
import { Roles } from '../../shared/decorators/roles.decorator';

@Controller('activity')
export class ActivityController {
  constructor(private readonly activityService: ActivityService) {}

  @Get('active')
  findAllActive(): Promise<Activity[]> {
    return this.activityService.findAllActive();
  }

  @Get(':id')
  findById(@Param('id') id: number): Promise<Activity> {
    return this.activityService.findById(id);
  }

  @Get()
  findAll(): Promise<Activity[]> {
    return this.activityService.findAll();
  }

  @Post()
  create(
    @Body(new ValidationPipe()) activity: ActivityDTO,
  ): Promise<ActivityDTO> {
    return this.activityService.create(activity);
  }

  @Delete(':id')
  delete(@Param('id', new ParseIntPipe()) id: number): Promise<Activity> {
    return this.activityService.delete(id);
  }

  @Put()
  update(@Body(new ValidationPipe()) activity: ActivityUpdateDto) {
    return this.activityService.update(activity);
  }
}
