import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { ActivityController } from './activity.controller';
import { ActivityService } from './services/activity.service';
import { CECAService } from '../../shared/services/ceca.service';
import { DatabaseModule } from '../database/database.module';
import { EnvModule } from '../env/env.module';
import { activityProviders } from './entities/activity.providers';

@Module({
  imports: [DatabaseModule, EnvModule],
  controllers: [ActivityController],
  providers: [ActivityService, CECAService, ...activityProviders],
  exports: [],
})
export class ActivityModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    /*      consumer
      .apply(AuthMiddleware)
      .with(ROUTES_EXCEPT_USER)
      .forRoutes(UserController); */
  }
}
