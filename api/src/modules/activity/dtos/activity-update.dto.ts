import { IsInt, IsNotEmpty } from 'class-validator';
import { ActivityDTO } from './activity.dto';

export class ActivityUpdateDto extends ActivityDTO {
  @IsInt()
  @IsNotEmpty()
  id: number;
}