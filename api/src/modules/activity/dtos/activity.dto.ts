import { IsInt, IsNotEmpty, IsNumber, IsString, Max, MaxLength } from "class-validator";

export class ActivityDTO {
  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  name: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(4000)

  description: string;

  @IsInt()
  @Max(2147483646)
  @IsNotEmpty()

  inscription_date_start: number;
  @IsInt()
  @Max(2147483646)
  @IsNotEmpty()
  inscription_date_end: number;

  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  type: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  paymethod: string;

  prices;
}