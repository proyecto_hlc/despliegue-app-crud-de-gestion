import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ActivityStatus } from '../models/activity-status.enum';
import { Enroll } from './enroll.entity';
import { Price } from './price.entity';

@Entity()
export class Activity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false, length: 255})
  name: string;

  @Column({ nullable: false, length: 4000})
  description: string;

  @Column({ nullable: false})
  type: string;

  @Column({ nullable: false })
  inscription_date_start: number;

  @Column({ nullable: false })
  inscription_date_end: number;

  @Column({ nullable: false })
  paymethod: string; // TODO-ENUM

  @Column({ type: 'enum', enum: ActivityStatus, default: ActivityStatus.ENABLED })
  status: ActivityStatus;

  @OneToMany(
    type => Price,
    price => price.activity,
    {cascade: true }
  )
  prices: Price[];

  @OneToMany(
    type => Enroll,
    enroll => enroll.activity,
    {cascade: true }
  )
  enrollees: Enroll[];

  
}