import {
  ACTIVITY_REPOSITORY_TOKEN,
  DB_CONNECTION_TOKEN,
  ENROLL_REPOSITORY_TOKEN,
} from '../../../shared/config/database.tokens.constants';

import { ActivityRepository } from './activity.repository';
import { Connection } from 'typeorm';
import { EnrollRepository } from './enroll.repository';

export const activityProviders = [
  {
    provide: ACTIVITY_REPOSITORY_TOKEN,
    useFactory: (connection: Connection) =>
      connection.getCustomRepository(ActivityRepository),
    inject: [DB_CONNECTION_TOKEN],
  },
  {
    provide: ENROLL_REPOSITORY_TOKEN,
    useFactory: (connection: Connection) =>
      connection.getCustomRepository(EnrollRepository),
    inject: [DB_CONNECTION_TOKEN],
  },
];
