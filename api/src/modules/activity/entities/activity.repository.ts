import * as moment from 'moment';

import { EntityRepository, Repository } from 'typeorm';

import { Activity } from './activity.entity';

@EntityRepository(Activity)
export class ActivityRepository extends Repository<Activity> {
  findAllActive() {
    const now = moment().unix();
    return this.createQueryBuilder('activity')
      .leftJoinAndSelect('activity.prices', 'prices')
      .where('activity.inscription_date_end > :now', { now })
      .orderBy('activity.name', 'ASC')
      .getMany();
  }
}
