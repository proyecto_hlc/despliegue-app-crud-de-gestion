import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { PayStatus } from "../models/pay-status.enum";
import { Activity } from "./activity.entity";

@Entity()
export class Enroll {
  @PrimaryGeneratedColumn()
  id: number;

  @Column( { nullable: false})
  license: number;

  @Column({ nullable: false})
  name: string;

  @Column( {nullable: false})
  pay_date: number;

  @Column({ nullable: false })
  price_name: string;

  @Column({ nullable: false, type: 'numeric' })
  price: number;

  @Column({ nullable: false })
  paymethod: string;

  @Column({ nullable: false })
  hash_pay: string;

  @Column({ type: 'enum', enum: PayStatus, default: PayStatus.PENDING })
  status: PayStatus;

  @ManyToOne(
    type => Activity,
    activity => activity.enrollees,
    { onUpdate: 'CASCADE', onDelete: 'CASCADE', eager: true })
    activity: Activity;

}