import * as moment from 'moment';

import { EntityRepository, Repository } from 'typeorm';

import { Enroll } from './enroll.entity';
import { PayStatus } from '../models/pay-status.enum';

@EntityRepository(Enroll)
export class EnrollRepository extends Repository<Enroll> {
  enrollUser({ user, activity, hash = 'FREE', status = PayStatus.PENDING }) {
    const enroll = {
      activity,
      status,
      license: user.license,
      name: user.name,
      pay_date: moment().unix(),
      paymethod: activity.paymethod,
      price_name: activity.price.name,
      price: +activity.price.amount,
      hash_pay: hash,
    };
    return this.save(enroll);
  }
  findByHash(hash: string): Promise<Enroll> {
    return this.findOne({
      where: {
        hash_pay: hash,
      },
      relations: ['activity'],
    });
  }
}
