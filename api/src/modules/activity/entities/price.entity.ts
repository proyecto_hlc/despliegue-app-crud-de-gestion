import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Activity } from "./activity.entity";

@Entity()
export class Price {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false  })
  name: string;

  @Column({ nullable: false, type: 'decimal' })
  amount: number;

  @ManyToOne(
    type => Activity,
    activity => activity.prices,
    { onUpdate: 'CASCADE', onDelete: 'CASCADE'}
  )
  activity: Activity;

}