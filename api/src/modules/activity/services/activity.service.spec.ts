import { Test } from '@nestjs/testing';
import { ActivityService } from './activity.service';
import {
  ACTIVITY_REPOSITORY_TOKEN,
  ENROLL_REPOSITORY_TOKEN,
} from '../../../shared/config/database.tokens.constants';
import { Repository } from 'typeorm';
import { Activity } from '../entities/activity.entity';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { CECAService } from '../../../shared/services/ceca.service';
import { ENV } from '../../../env';
import { ActivityStatus } from '../models/activity-status.enum';

describe('ActivityService', () => {
  let activityService;
  let activityRepository;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [],
      providers: [
        ActivityService,
        {
          provide: ACTIVITY_REPOSITORY_TOKEN,
          useFactory: () => ({
            findOne: () => true,
            find: () => true,
            save: () => true,
          }),
        },
        {
          provide: ENROLL_REPOSITORY_TOKEN,
          useFactory: () => ({}),
        },
        {
          provide: MailerService,
          useValue: {
            mailerServiceData: jest.fn(),
          },
        },
        CECAService,
        ENV,
      ],
    }).compile();

    activityService = moduleRef.get<ActivityService>(ActivityService);
    activityRepository = moduleRef.get<Repository<Activity>>(
      ACTIVITY_REPOSITORY_TOKEN,
    );
  });

  describe('findById', () => {
    it('should return an activity using the ID', async () => {
      const params = {
        id: 1,
        options: {
          relations: ['prices', 'enrollees'],
        },
      };
      const mock = {
        activity: {
          id: 1,
          name: 'Actividad1',
          prices: [],
        } as Activity,
      };
      jest
        .spyOn(activityRepository, 'findOne')
        .mockImplementation(() => mock.activity);

      const activity = await activityService.findById(params.id);

      expect(activity).toBe(mock.activity);
      expect(activityRepository.findOne).toHaveBeenCalledWith(
        params.id,
        params.options,
      );
    });

    it('should return an exception when activity is not found', async () => {
      const params = {
        id: 1,
        options: {
          relations: ['prices', 'enrollees'],
        },
      };
      const mock = {
        activity: null as Activity,
      };
      jest
        .spyOn(activityRepository, 'findOne')
        .mockImplementation(() => mock.activity);

      try {
        await activityService.findById(params.id);
      } catch (e) {
        expect(e).toBeInstanceOf(NotFoundException);
      }

      expect(activityRepository.findOne).toHaveBeenCalledWith(
        params.id,
        params.options,
      );
    });
  });

  describe('findAll', () => {
    it('should return all activities', async () => {
      const mock = {
        activities: [
          {
            id: 1,
            name: 'Actividad1',
          } as Activity,
          {
            id: 2,
            name: 'Actividad2',
          } as Activity,
          {
            id: 3,
            name: 'Actividad3',
          } as Activity,
        ],
      };
      jest
        .spyOn(activityRepository, 'find')
        .mockImplementation(() => mock.activities);

      const activities = await activityService.findAll();

      expect(activities).toBe(mock.activities);
      expect(activityRepository.find).toBeCalled();
    });

    it('should return an empty array when there are no activities', async () => {
      const mock = {
        activities: [],
      };
      jest
        .spyOn(activityRepository, 'find')
        .mockImplementation(() => mock.activities);

      const activities = await activityService.findAll();

      expect(activities).toBe(mock.activities);
      expect(activityRepository.find).toBeCalled();
    });
  });

  describe('create', () => {
    it('should insert an activity', async () => {
      const params = {
        name: 'Actividad2',
      } as Activity;

      const mockActivity = {
        activity: {
          id: 2,
          name: 'Actividad2',
        } as Activity,
      };

      const mockBefore = {
        activities: [
          {
            id: 1,
            name: 'Actividad1',
          } as Activity,
        ],
      };

      const mockAfter = {
        activities: [
          {
            id: 1,
            name: 'Actividad1',
          } as Activity,
          {
            id: 2,
            name: 'Actividad2',
          } as Activity,
        ],
      };
      jest
        .spyOn(activityRepository, 'find')
        .mockImplementation(() => mockBefore.activities);

      const activitiesBefore = await activityService.findAll();

      expect(activitiesBefore).toBe(mockBefore.activities);

      jest
        .spyOn(activityRepository, 'save')
        .mockImplementation(() => mockActivity.activity);
      jest
        .spyOn(activityRepository, 'find')
        .mockImplementation(() => mockAfter.activities);

      const activity = await activityService.create(params);
      const activitiesAfter = await activityService.findAll();

      expect(activity).toBe(mockActivity.activity);
      expect(activitiesAfter).toBe(mockAfter.activities);
      expect(activityRepository.save).toHaveBeenCalledWith(params);
    });

    describe('delete', () => {
      it('should return an activity with the status changed', async () => {
        const params = {
          id: 2,
        };

        const mockActivity = {
          activity: {
            id: 2,
            name: 'Actividad2',
            status: 'DISABLED',
          } as Activity,
        };

        const mockBefore = {
          activities: [
            {
              id: 1,
              name: 'Actividad1',
              status: 'ENABLED',
            } as Activity,
            {
              id: 2,
              name: 'Actividad2',
              status: 'ENABLED',
            } as Activity,
            {
              id: 3,
              name: 'Actividad3',
              status: 'ENABLED',
            } as Activity,
          ],
        };

        const mockAfter = {
          activities: [
            {
              id: 1,
              name: 'Actividad1',
              status: 'ENABLED',
            } as Activity,
            {
              id: 2,
              name: 'Actividad2',
              status: 'DISABLED',
            } as Activity,
            {
              id: 3,
              name: 'Actividad3',
              status: 'ENABLED',
            } as Activity,
          ],
        };

        jest
          .spyOn(activityRepository, 'find')
          .mockImplementation(() => mockBefore.activities);

        const activitiesBefore = await activityService.findAll();

        expect(activitiesBefore).toBe(mockBefore.activities);

        jest
          .spyOn(activityRepository, 'findOne')
          .mockImplementation(() => mockBefore.activities[1]);
        jest
          .spyOn(activityRepository, 'save')
          .mockImplementation(() => mockActivity.activity);
        jest
          .spyOn(activityRepository, 'find')
          .mockImplementation(() => mockAfter.activities);

        const activity = await activityService.delete(params.id);
        const activitiesAfter = await activityService.findAll();

        expect(activity).toBe(mockActivity.activity);
        expect(activitiesAfter).toBe(mockAfter.activities);
        expect(activityRepository.findOne).toHaveBeenCalledWith(params.id);
        expect(activityRepository.save).toHaveBeenCalledWith(
          mockBefore.activities[1],
        );
      });

      it('should return a not found exception when activity is not found', async () => {
        const params = {
          id: 5,
        };

        const mock = {
          activity: null,
        };

        jest
          .spyOn(activityRepository, 'findOne')
          .mockImplementation(() => mock.activity);

        try {
          await activityService.delete(params.id);
        } catch (e) {
          expect(e).toBeInstanceOf(NotFoundException);
        }

        expect(activityRepository.findOne).toHaveBeenCalledWith(params.id);
      });

      it('should return a bad request exception when activity is in DISABLED status', async () => {
        const params = {
          id: 1,
        };

        const mock = {
          activity: {
            id: 1,
            name: 'Actividad1',
            status: 'DISABLED',
          } as Activity,
        };

        jest
          .spyOn(activityRepository, 'findOne')
          .mockImplementation(() => mock.activity);

        try {
          await activityService.delete(params.id);
        } catch (e) {
          expect(e).toBeInstanceOf(BadRequestException);
        }

        expect(activityRepository.findOne).toHaveBeenCalledWith(params.id);
      });
    });

    describe('update', () => {
      it('should update an activity and return it', async () => {
        const params = {
          activity: {
            id: 1,
            name: 'Escalada',
          } as Activity,
        };

        const mock = {
          activity: {
            id: 1,
            name: 'Rappel y escalada',
          } as Activity,
        };

        jest
          .spyOn(activityRepository, 'save')
          .mockImplementation(() => mock.activity);

        const activity = await activityService.update(params.activity);

        expect(activity).toBe(params.activity);
        expect(activityRepository.save).toHaveBeenCalledWith(params.activity);
      });

      it('should return an exception when the update fails', async () => {
        const params = {
          activity: {
            id: 1,
            name: 'Escalada',
          } as Activity,
        };
        jest
          .spyOn(activityRepository, 'save')
          .mockImplementation(() => new Error());

        try {
          await activityService.update(params.activity);
        } catch (e) {
          expect(e).toBeInstanceOf(BadRequestException);
        }

        expect(activityRepository.save).toHaveBeenCalledWith(params.activity);
      });
    });
  });
});
