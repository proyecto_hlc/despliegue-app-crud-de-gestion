import { Activity } from '../entities/activity.entity';
import { ActivityRepository } from '../entities/activity.repository';
import {
  Injectable,
  Inject,
  NotFoundException,
  ForbiddenException,
  BadRequestException,
} from '@nestjs/common';
import {
  ACTIVITY_REPOSITORY_TOKEN,
  ENROLL_REPOSITORY_TOKEN,
} from '../../../shared/config/database.tokens.constants';
import { EnrollRepository } from '../entities/enroll.repository';

import { PayStatus } from '../models/pay-status.enum';
import { MailerService } from '@nestjs-modules/mailer';

import { CECAService } from '../../../shared/services/ceca.service';
import { ActivityUpdateDto } from '../dtos/activity-update.dto';
import { ActivityDTO } from '../dtos/activity.dto';
import { ActivityStatus } from '../models/activity-status.enum';

@Injectable()
export class ActivityService {
  constructor(
    @Inject(ACTIVITY_REPOSITORY_TOKEN)
    private readonly activityRepository: ActivityRepository,
    @Inject(ENROLL_REPOSITORY_TOKEN)
    private readonly enrollRepository: EnrollRepository,
    private readonly mailerService: MailerService,
    private readonly cecaService: CECAService,
  ) {}

  findAllActive(): Promise<Activity[]> {
    return this.activityRepository.findAllActive();
  }

  async enrollUserInFreeActivity({ user, activity }) {
    /* TODO (Descomentar en producción) */
    // checkUserEnrolled(user, activity);
    await this.enrollRepository.enrollUser({
      activity,
      user,
      status: PayStatus.VALID,
    });
    //this.sendEmailEnroll({ user, activity }); //TODO (Production ok)
    return { url: '/enroll-ok' };
  }
  private async checkUserEnrolled(user, activity) {
    const userEnrolled = await this.enrollRepository.findOne({
      activity,
      name: user.name,
    });
    if (userEnrolled) {
      throw new ForbiddenException('User Registered');
    }
  }

  async enrollUser({ user, activity }) {
    /* TODO (Descomentar en producción) */
    // checkUserEnrolled(user, activity);

    // this.sendEmailEnroll({ user, activity }); // TODO-Borrarlo de aquí pues es para cuando la comunicación de CECA

    const { url, hash } = this.cecaService.generateURL(activity);
    await this.enrollRepository.enrollUser({ activity, user, hash });
    return { url };
  }

  async findById(_id: number): Promise<Activity> {
    const activity = await this.activityRepository.findOne(_id, {
      relations: ['prices', 'enrollees'],
    });
    if (!activity) {
      throw new NotFoundException(
        `Activity Service: Activity not found! ID SEARCHED = ${_id}`,
      );
    }
    return activity;
  }

  findAll(): Promise<Activity[]> {
    return this.activityRepository.find({
      order: {
        inscription_date_start: 'DESC',
      },
    });
  }

  create(activity: ActivityDTO): Promise<ActivityDTO> {
    return this.activityRepository.save(activity);
  }

  async delete(activityID: number): Promise<Activity> {
    const activityToDelete = await this.activityRepository.findOne(activityID);
    if (!activityToDelete) {
      throw new NotFoundException("Couldn't find activity.");
    }
    if (activityToDelete.status === ActivityStatus.DISABLED) {
      throw new BadRequestException('Activity already disabled.');
    }
    activityToDelete.status = ActivityStatus.DISABLED;
    return this.activityRepository.save(activityToDelete);
  }

  async update(activity: ActivityUpdateDto): Promise<ActivityUpdateDto> {
    let resultUpdatedActivity;
    try {
      resultUpdatedActivity = await this.activityRepository.save(activity);
    } catch (e) {
      throw new BadRequestException('Modificación no realizada');
    }
    return activity;
  }
}
