import { DB_CONNECTION_TOKEN } from '../../shared/config/database.tokens.constants';
import { createConnection } from 'typeorm';
export const databaseProviders = [
  {
    provide: DB_CONNECTION_TOKEN,
    useFactory: async () =>
      await createConnection({
        type: 'postgres',
        host: 'db',
        port: 5432,
        username: 'root',
        password: 'toor',
        database: 'happyday',
        entities: [__dirname + '/../**/*entity.{ts,js}'],
        synchronize: true,
        logging: true,
      }),
  },
];
