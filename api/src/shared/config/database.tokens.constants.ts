export const USER_REPOSITORY_TOKEN = 'UserRepositoryToken';
export const ACTIVITY_REPOSITORY_TOKEN = 'ActivityRepositoryToken';
export const ENROLL_REPOSITORY_TOKEN = 'EnrollRepositoryToken';
export const PRICE_REPOSITORY_TOKEN = 'PriceRepositoryToken';
export const DB_CONNECTION_TOKEN = 'DbConnectionToken';
