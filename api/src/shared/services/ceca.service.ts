import { ENV } from '../../env';
import { Injectable } from '@nestjs/common';

const crypto = require('crypto');

@Injectable()
export class CECAService {
  constructor(private readonly env: ENV) {}

  generateURL(activity) {
    const hash = crypto.randomBytes(20).toString('hex');
    const importe = +activity.price.amount * 100;

    const seed = `${this.env.CECA.key}${this.env.CECA.MerchantID}${this.env.CECA.AcquirerBIN}${this.env.CECA.TerminalID}${hash}${importe}${this.env.CECA.TipoMoneda}${this.env.CECA.Exponente}${this.env.CECA.Cifrado}${this.env.CECA.URL_OK}${this.env.CECA.URL_NOK}`;

    const Firma = crypto
      .createHash('sha256')
      .update(seed)
      .digest('hex');

    const url = `${this.env.CECA.CECA_URL}?MerchantID=${this.env.CECA.MerchantID}&AcquirerBIN=${this.env.CECA.AcquirerBIN}&TerminalID=${this.env.CECA.TerminalID}&Num_operacion=${hash}&Importe=${importe}&TipoMoneda=${this.env.CECA.TipoMoneda}&Exponente=${this.env.CECA.Exponente}&URL_OK=${this.env.CECA.URL_OK}&URL_NOK=${this.env.CECA.URL_NOK}&Firma=${Firma}&Cifrado=${this.env.CECA.Cifrado}&Idioma=${this.env.CECA.Idioma}`;
    return {
      hash,
      url,
    };
  }

  isValidConfirmEnroll(CECA): boolean {
    const { Num_operacion, Importe, Referencia, Firma } = CECA;
    const seed = `${this.env.CECA.key}${this.env.CECA.MerchantID}${this.env.CECA.AcquirerBIN}${this.env.CECA.TerminalID}${Num_operacion}${Importe}${this.env.CECA.TipoMoneda}${this.env.CECA.Exponente}${Referencia}`;
    const hash = crypto
      .createHash('sha256')
      .update(seed)
      .digest('hex');
    return hash === Firma;
  }
}
