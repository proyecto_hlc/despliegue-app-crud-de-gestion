import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
  /* {
    path: 'dashboard',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    component: DashboardPageComponent,
  }, */
  {
    path: 'activity',
    /* pathMatch: 'full', */
    loadChildren: () =>
      import('./pages/activity/activity.module').then((m) => m.ActivityModule),
  },
  {
    path: '404',
    component: NotFoundComponent,
  },
  {
    path: '**',
    loadChildren: () =>
      import('./pages/activity/activity.module').then((m) => m.ActivityModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
