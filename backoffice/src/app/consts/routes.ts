export enum routes {
  DASHBOARD = '/dashboard',
  ACTIVITY = '/activity',
  LOGIN = '/login',
  ACTIVITY_TYPE = '/activity-type',
}
