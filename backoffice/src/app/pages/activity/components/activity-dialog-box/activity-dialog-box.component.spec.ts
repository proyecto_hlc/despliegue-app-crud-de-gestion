import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityDialogBoxComponent } from './activity-dialog-box.component';

describe('ActivityDialogBoxComponent', () => {
  let component: ActivityDialogBoxComponent;
  let fixture: ComponentFixture<ActivityDialogBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityDialogBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityDialogBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
