import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-activity-dialog-box',
  templateUrl: './activity-dialog-box.component.html',
  styleUrls: ['./activity-dialog-box.component.css']
})
export class ActivityDialogBoxComponent implements OnInit {


  constructor(
    public dialogRef: MatDialogRef<ActivityDialogBoxComponent>,
  ) { }

  ngOnInit(): void {
  }

  doAction() {
    this.dialogRef.close({
      event: 'ok'
    })
  }

  closeDialog(){
    this.dialogRef.close({ event:'ko' });
  }

}
