import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Activity } from 'src/app/shared/models/activity.interface';
import { ActivityService } from 'src/app/shared/services/activity.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ActivityDialogBoxComponent } from '../../components/activity-dialog-box/activity-dialog-box.component';
import * as moment from 'moment';


@Component({
  selector: 'app-activity-details',
  templateUrl: './activity-details.component.html',
  styleUrls: ['./activity-details.component.scss']
})
export class ActivityDetailsComponent implements OnInit {

  activity$: Observable<Activity>;

  constructor(
    public dialog: MatDialog,
    private readonly snackBarService: MatSnackBar,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly activityService: ActivityService,
  ) { }

  ngOnInit(): void {
   

    this.activity$ = this.activatedRoute.paramMap.pipe(
      switchMap(params => {
        const selectedId = Number(params.get('id'));
        return this.activityService.findOneById(selectedId);
      })
    );
    this.activity$.subscribe(() => {
   
    })
  }
  
  save(activity: Activity) {
    
    this.activityService.update(activity).subscribe(
      () => {
        this.showSnackBar({
          message: 'Operación realizada satisfactoriamente',
          title: 'ÉXITO',
        });
        this.router.navigateByUrl('/activity');
      },
      () => {
        this.showSnackBar({
          message: 'Operación no realizada debido a problemas con el servidor',
          title: 'ERROR',
        });
      },
    );
  }

  openDeactivateDialog(action, activity) {
    console.log(activity)
    const dialogRef = this.dialog.open(ActivityDialogBoxComponent, {
      width: '350px',
      data:activity
    });
    dialogRef.afterClosed().subscribe(status => {
      if(status.event === 'ok') {
        this.activityService.delete(activity.id).subscribe(
          () => {
            this.showSnackBar({
              message: 'Operación realizada satisfactoriamente',
              title: 'ÉXITO',
            });
            this.router.navigateByUrl('/activity');
          },
          (error) => {
            this.showSnackBar({
              message: 'Operación no realizada debido a problemas con el servidor',
              title: 'ERROR',
            });
          },
        )
      }
    })    
}

isEnrolleesEmpty(activity) {
  return activity.enrollees.length === 0
}

isCompleted(activity) {
  return activity.inscription_date_end < Date.now()/1000;
}



  showSnackBar({ message, title }) {
    this.snackBarService.open(message, title, {
      duration: 4000,
    });
  }
  

}
